/* :: MENU :: */
$(document).ready(function () {
    if ((typeof WOW) === 'function') {

        var wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: true,
            live: true,
        });

        wow.init();
    }

    $(".toggle").click(function () {
        if ($("body").hasClass('open')) {
            $("body").removeClass("open");
        }
        else{
            $("body").addClass("open");
        }
    });
    if ($("#register").hasClass("active")) {
        var popup = $("#register").closest(".popup"),
            close = popup.find(".btn-close");
        close.hide();
    }
    $(".btn-register").click(function(){
        $(".popup-register").fadeIn();
    });
    $('input[type="tel"]').bind('keyup', function (e) {
        e.target.value = e.target.value.replace(/[^\d]/g,'');
            return false;
    });
    $('#register form').submit(function(e){
        e.preventDefault();
        if(!($('.label-checkbox input').is(':checked'))) {
            $(".aleat-regis").html("กรุณากรอกข้อมูลลงทะเบียนให้ครบถ้วน ก่อนส่งภาพเข้าประกวดด้วยค่ะ")
        }
        else{
            $(".aleat-regis").html("");
        }
    });
});
function openLoad() {
    $("body").addClass("open-load");
}
function closeLoad(){
    $("body").removeClass("open-load");
}