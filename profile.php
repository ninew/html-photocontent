<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <!-- include meta header -->
    <?php include 'ssi/meta.php'; ?>
    <link rel="stylesheet" href="css/gallery.css" type="text/css">
    <script>
        $(document).ready(function () {
            $(".box-item").each(function(){
                var close = $(".btn-close");
                close.click(function(){
                    $("#popup-del").show();
                    $(".popup-alert").fadeIn();
                });
            })

            $(".popup-alert .btn-close, .popup-alert .mesk, .popup-alert .cancel").click(function(){
                $(".popup-alert").fadeOut();
                $(".popup").delay( 600 ).hide();
            });
        });
    </script>
</head>
<body class="page-landing page-landing-blur">
    <?php include 'ssi/header.php'; ?>
    <main class="container">
        <h1 class="header-gallery">คุณ Jeab Noppadol</h1>
        <div class="profile-action">
            <p>คุณสามารถส่งภาพเข้าประกวดได้อีก 1 ภาพ</p>
            <div class="status">
                <a href="upload.php" class="btn-upload">ส่งภาพเข้าประกวด</a>
            </div>
        </div>
        <div class="gallery-layout">
            <?php for ($i = 0; $i < 3; $i++) { ?>
                <div class="box-item">
                    <a class="btn-close"></a>
                    <div class="box-img">
                        <img src="img/test/_thumb.jpg">
                    </div>
                    <div class="box-text">
                        <div class="dt-caption">
                            <p>
                                ใช้มา 10 ปี แล้ว เลือกไม่ผิดจริงๆ ใช้แล้วคุ้มค่ามาก อีซูซุ...ใช้แล้วแฮปปี้สุดๆ
                            </p>
                        </div>
                        <p>
                            จากคุณ
                            <span>Piyanuch</span>
                        </p>
                    </div>
                    <div class="box-action">
                        <button class="btn-vote">โหวต</button>
                        <p>13520</p>
                        <a class="btn-share">แชร์</a>
                        <div class="data">
                            <p>
                                No.1111
                            </p>
                            <p>
                                วันที่อัพโหลด 22/06/2
                            </p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </main>
    <div class="popup-alert">
        <div class="mesk"></div>
        <div class="popup" id="popup-del">
            <a class="btn-close"></a>
            <p>
                เมื่อคุณทำการลบภาพ ข้อมูลรูปภาพ , ชื่อภาพ และจำนวนผลโหวตทั้งหมด จะถูกลบทิ้งออกจากระบบ
                <b>คุณยืนยันที่จะลบภาพนี้หรือไม่?</b>
            </p>
            <div class="box-link">
                <a class="btn-del">ตกลง</a>
                <a class="cancel">ยกเลิก</a>    
            </div>
        </div>
        <div class="popup" id="popup-vote">
            <a class="btn-close"></a>
            <p></p>
        </div>
    </div>    
</body>

</html>