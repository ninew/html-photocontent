<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <!-- include meta header -->
    <?php include 'ssi/meta.php'; ?>
    <link rel="stylesheet" href="css/gallery.css" type="text/css">
    <script>
        $(document).ready(function () {
            $(".gallery-item-img").each(function(){
                var img = $(this).data("img"),
                    caption = $(this).data("caption"),
                    popup = $(".popup-gallery"),
                    pic = popup.find(".box-img img"),
                    txt = popup.find(".dt-caption p");

                $(this).click(function(){
                    $("#view").show();
                    popup.fadeIn();
                    pic.attr('src', img);
                    txt.html(caption);
                })
                $(".popup-gallery .btn-close, .popup-gallery .mesk").click(function(){
                    $(".popup-gallery").fadeOut();
                    pic.attr('src', '');
                    $(".popup").delay( 600 ).hide()
                });
            });
            $(".btn-vote").each(function(){
                $(this).click(function(){
                    $("#thk").show();
                    $("#view").hide();
                    $(".popup-gallery").fadeIn();
                })
                if (true) {}
            });
        });
    </script>
</head>
<body class="page-landing page-landing-blur">
<?php include 'ssi/header.php'; ?>
    <main class="gallery">
        <h1 class="header-gallery">ภาพถ่ายทั้งหมด</h1>
        <div class="gallery-form">
            <div class="gallery-form-item">
                <select>
                    <option>เรียงลำดับ คะแนนโหวตสุงสุด</option>

                </select>
            </div>
            <div class="gallery-form-item">
                <form action="http://isuzu60th.local/isuzu60th/gallery/1" method="post">
                    <input type="search" name="s" id="s" placeholder="ค้นหา" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ค้นหา...'" value="">
                    <input type="submit" value="&#xe904;">
                </form>
            </div>
        </div>
        <div class="gallery-layout">
            <?php for ($i = 0; $i < 8; $i++) { ?>
                <div class="link-gallery">
                    <article class="gallery-item">
                        <a class="gallery-item-img" data-img="img/test/_thumb.jpg" data-caption="ใช้มา 10 ปี แล้ว เลือกไม่ผิดจริงๆ ใช้แล้วคุ้มค่ามาก อีซูซุ...ใช้แล้วแฮปปี้สุดๆ">
                            <img src="img/test/_thumb.jpg" alt="" data-img="img/test/_thumb.jpg">
                        </a>
                        <div class="gallery-item-content">
                            <h1>Jeab Noppadol</h1>
                            <p>13520</p>
                            <button class="btn-vote">โหวต</button>
                        </div>
                    </article>
                </div>
            <?php } ?>
        </div>
        <a class="btn-view-more" href="#">ดูเพิ่ม</a>
    </main>
    <div class="popup-gallery">
        <div class="mesk"></div>
        <div class="popup" id="view">
            <a class="btn-close"></a>
            <div class="box-img">
                <img src="">
            </div>
            <div class="box-text">
                <div class="dt-caption">
                    <p></p>
                </div>
                <p>
                    จากคุณ
                    <span>Piyanuch</span>
                </p>    
            </div>
            
            <div class="box-action">
                <button class="btn-vote">โหวต</button>
                <p>13520</p>
                <a class="btn-share">แชร์</a>
                <div class="data">
                    <p>
                        No.1111
                    </p>
                    <p>
                        วันที่อัพโหลด 22/06/2
                    </p>
                </div>
            </div>
        </div>
        <div class="popup" id="thk">
            <a class="btn-close"></a>
            <p>
                <b>โหวตเรียบร้อยแล้วค่ะ</b>
                คุณสามารถโวตรูปนี้ได้อีกครั้งในวันพรุ่งนี้คะ
            </p>
            <a class="btn-share">แชร์</a>
        </div>
    </div>
</div>
</body>

</html>