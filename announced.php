<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <!-- include meta header -->
    <?php include 'ssi/meta.php'; ?>
    <link rel="stylesheet" href="css/gallery.css" type="text/css">
    <script>
        $(document).ready(function () {
        
        });
    </script>
</head>
<body class="page-landing page-landing-blur">
<?php include 'ssi/header.php'; ?>
    <main class="container">
        <h1 class="header-gallery">Photos of the Week</h1>
        <h2>รางวัลพิเศษจากการตัดสินของคณะกรรมการ รับของที่ระลึกจากอีซูซุ</h2>
        <div class="gallery-layout">
            <?php for ($i = 0; $i < 3; $i++) { ?>
                <article class="item-announced">
                    <h1>
                       สัปดาห์ที่ 3
                       <span>วันที่ 16-22 มิถุนายน 2560</span> 
                    </h1>
                    <div class="box-item">
                        <a class="btn-close"></a>
                        <div class="box-img">
                            <img src="img/test/_thumb.jpg">
                        </div>
                        <div class="box-text">
                            <div class="dt-caption">
                                <p>
                                    ใช้มา 10 ปี แล้ว เลือกไม่ผิดจริงๆ ใช้แล้วคุ้มค่ามาก อีซูซุ...ใช้แล้วแฮปปี้สุดๆ
                                </p>
                            </div>
                            <p>
                                จากคุณ
                                <span>Piyanuch</span>
                            </p>
                        </div>
                        <div class="box-action">
                            <b>จำนวนโหวต<span>13520</span></b>
                            <a class="btn-share">แชร์</a>
                            <div class="data">
                                <p>
                                    No.1111
                                </p>
                                <p>
                                    วันที่อัพโหลด 22/06/2
                                </p>
                            </div>
                        </div>
                    </div>
                </article>
            <?php } ?>
        </div>
    </main>
</div>
</body>

</html>