<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="preload">
    <div class='uil-ring-css'><div class="ring"></div></div>
</div>
<header class="header">
    <div class="container">
        <div class="toggle"><span></span></div>
        <a href="index.php" class="logo"><img src="img/logo.png"></a>
        <nav>
            <ul>
                <li>
                    <a href="index.php">หน้าแรก</a>
                </li>
                <li>
                    <a href="upload.php">ส่งภาพเข้าประกวด</a>
                </li>
                <li>
                    <a href="gallery.php">ภาพถ่ายทั้งหมด</a>
                </li>
                <li>
                    <a href="announced.php">Photos of the Week</a>
                </li>
                <li>
                    <a href="rules.php">กติกาและของรางวัล</a>
                </li>
            </ul>
        </nav>
        <nav class="nav-member">
            <ul>
                <li class="box-login">
                    <a>ลงชื่อเข้าใช้งาน</a>                    
                    <!-- <span>
                    <a href="profile.php">Piyanuch</a> 
                    <a class="user"></a>
                    </span> -->
                </li>
                <li>
                    <a href="https://www.facebook.com/sharer/sharer.php?app_id=[your_app_id]&sdk=joey&u=[full_article_url]&display=popup&ref=plugin&src=share_button"  OnClick="window.open('http://www.facebook.com/share.php?u=demo.gooddigi.in.th/photocontent/', '_blank', 'width=400,height=500');void(0);">แชร์</a>
                </li>
            </ul>
        </nav>    
    </div>    
</header>
<div class="popup-register">
    <div class="mesk"></div>
    <div class="popup">
        <div id="login-fb">
            <p>เข้าสู่ระบบด้วย facebook</p>
            <a class="btn-fb">login with facebook</a>
            <a href="index.php">กลับสู่หน้าแรก</a>
        </div>
        <div id="register" class="active">
            <strong>กรุณากรอกข้อมูลลงทะเบียนให้ครบถ้วน<br>ก่อนส่งภาพเข้าประกวดด้วยค่ะ</strong>
            <form>
                <label>
                    ชื่อ-นามสกุล
                    <input type="text"></input>
                </label>
                <label>
                    อีเมล์
                    <input type="email"></input>
                </label>
                <label>
                    เบอร์โทรศัพท์
                    <input type="tel"></input>
                </label>
                <label>
                    ที่อยู่ปัจจุบัน
                    <input type="text"></input>
                </label>
                <p class="remark">
                    หมายเหตุ : กรุณากรอกข้อมูลให้ครบถ้วนและถูกต้องตรงตามความเป็นจริง
                    เพื่อประโยชน์ของท่านในการติดต่อรับของรางวัล
                </p>
                <label class="label-checkbox">
                    <input type="checkbox"></input>
                    <span></span>
                    <p>
                        ข้าพเจ้าได้อ่านและยอมรับกติกาในการสมัครเรียบร้อยแล้ว<a href="rules.php" target="_blank">คลิกเพื่ออ่านกติกา</a>
                    </p>                
                </label>
                <a href="index.php">กลับสู่หน้าแรก</a>
                <input type="submit" value="ยืนยันการสมัคร"></input>
                <span class="aleat-regis"></span>
            </form>    
        </div>
        
    </div>
</div>
<a href="#" class="logo-tripetch"><img src="img/logo-tripetch.png"></a>