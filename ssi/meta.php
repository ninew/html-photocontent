<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>ISUZU PHOTO CONTEST มีความสุขให้โลกเห็น...ส่งภาพเด็ดๆ มาประชันกัน 60 ปี อีซูซุ แฮปปี้สุดสุด..!</title>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<meta name="keyword" content="isuzu">
<meta name="description" content="มาฉลองความสุขร่วมกันกับกิจกรรมถ่ายภาพภายใต้หัวข้อ “โฟกัสความสุข ฉลอง 60 ปีอีซูซุ”">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<meta name="og:title" content="ISUZU PHOTO CONTEST มีความสุขให้โลกเห็น...ส่งภาพเด็ดๆ มาประชันกัน 60 ปี อีซูซุ แฮปปี้สุดสุด..!">
<meta name="og:description" content="มาฉลองความสุขร่วมกันกับกิจกรรมถ่ายภาพภายใต้หัวข้อ “โฟกัสความสุข ฉลอง 60 ปีอีซูซุ”">
<meta property="og:type" content="website">
<meta property="og:url" content="http://www.isuzuphotocontest.com/">
<meta property="og:image" content="img/share.jpg">
<meta property="og:site_name" content="isuzuphotocontest">
<meta property="og:title" content="ISUZU PHOTO CONTEST มีความสุขให้โลกเห็น...ส่งภาพเด็ดๆ มาประชันกัน 60 ปี อีซูซุ แฮปปี้สุดสุด..!">
<meta property="og:description" content="มาฉลองความสุขร่วมกันกับกิจกรรมถ่ายภาพภายใต้หัวข้อ “โฟกัสความสุข ฉลอง 60 ปีอีซูซุ”">

<link rel="apple-touch-icon" sizes="57x57" href="img/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="img/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="img/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="img/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="img/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="img/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="img/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="img/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="img/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="img/fav/favicon-16x16.png">
<link rel="manifest" href="img/fav/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="img/fav/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">


<link rel="stylesheet" href="css/base.css" type="text/css">
<link rel="stylesheet" href="css/responsive.css" type="text/css">
<!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="js/global.js" type="text/javascript"></script>