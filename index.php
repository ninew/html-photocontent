<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <!-- include meta header -->
    <?php include 'ssi/meta.php'; ?>
    <link rel="stylesheet" href="css/home.css" type="text/css">
    <script>
        $(document).ready(function () {
        	
        });
    </script>
</head>
<body>
	<?php include 'ssi/header.php'; ?>
	<main class="page-home">
		<div class="center">
			<img src="img/title-home.png">
			<div class="title-txt">
				<img src="img/logo-60.png">
				<h1>
					มีความสุขให้โลกเห็น...
				</h1>
				<h2>
					ส่งภาพเด็ดๆ มาประชันกัน  60 ปี<br> อีซูซุ แฮปปี้สุดสุด..!
				</h2>
				<p>
					มาฉลองความสุขร่วมกันกับกิจกรรมถ่ายภาพภายใต้หัวข้อ
					<span>“โฟกัสความสุข<br> ฉลอง 60 ปีอีซูซุ”</span>
				</p>
			</div>
			<div class="title-detail">
				<p>
					ชวนเพื่อน ชวนพี่ ชวนน้อง รวมแกงค์ ร่วมก๊วน ดีไซน์ ถ่ายภาพความสุข ในรูปแบบต่างๆ
					<b><span>โดยถ่ายทอดผ่านรถอีซูซุ แบบไหน รุ่นใดก็ได้</span> ขอเพียงมีรถอีซูซุอยู่ในภาพ</b>
					พร้อมตั้งชื่อภาพโดนๆ จัดเต็ม! สุดพลัง! ไม่มียั้ง!...แล้วรอลุ้นรางวัลมากมาย 
				</p>
				<p>
					รวมมูลค่ากว่า<b>200,000<small>บาท</small></b>
				</p>
			</div>
			<div class="box-btn">
				<p>
					ร่วมส่งภาพเข้าประกวด และร่วมโหวตได้ที่นี่
				</p>
				<ul>
					<li>
						• ส่งภาพเข้าประกวด<br> 1 มิถุนายน - 31 สิงหาคม 2560
					</li>
					<li>
						 • ระยะเวลาร่วมโหวต<br> 1 มิถุนายน - 14 กันยายน 2560 เวลา 23.59 น.
					</li>
				</ul>
				<a class="btn-register">ส่งภาพเข้าประกวด</a>
				<a href="gallery.php">ร่วมโหวตภาพ</a>
			</div>
		</div>
	</main>
</body>

</html>