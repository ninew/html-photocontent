<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <!-- include meta header -->
    <?php include 'ssi/meta.php'; ?>
    <link rel="stylesheet" href="css/upload.css" type="text/css">
    <script>
        $(document).ready(function () {
        	function readURL(input) {
		        if (input.files && input.files[0]) {
		            var reader = new FileReader();
		            
		            reader.onload = function (e) {
		                $('#previewIMG').attr('src', e.target.result);
		            }
		            
		            reader.readAsDataURL(input.files[0]);
		        }
		    }
		    
		    $("#uploadIMG").change(function(){
		    	$(".box-img").addClass("show");
		    	$("#formUpload label").hide();
	    		$('#previewIMG').show();
		        readURL(this);
			    //$("#control-upload ul").fadeOut();
		    });
	    	var box = $(".box-photo"),
	    		boxIMG = $(".box-img"),
	    		boxTXT = $(".box-text"),
	    		caption = $("#control-caption"),
	    		preview = $("#control-preview"),
	    		textarea = $(".dt-caption textarea"),
	    		txt = $(".dt-caption p");

			$(".reset").click(function(){
	    		$('#uploadIMG').val("");
	    		$('#previewIMG').hide();
		    	$("#formUpload label").show();
	    		boxIMG.removeClass("show");
		    });
		    $("#control-upload .btn-next").click(function(){
		    	if (boxIMG.hasClass("show")) {
		    		$("#control-upload").hide();
		    		caption.delay( 600 ).fadeIn();
		    		box.delay( 600 ).addClass("box-caption");
		    		boxTXT.fadeIn();
		    	}
		    	else{
		    		$("#upload-alert").show();
		    		$(".popup-alert").fadeIn();
		    	}
		    });
		    $("#control-caption .btn-next").click(function(){
		    	if (textarea.val() != "") {
		    		box.addClass("box-preview");	    	
			    	txt.html(textarea.val());
			    	textarea.hide();	
			    	txt.fadeIn();
			    	caption.fadeOut();
			    	preview.delay( 600 ).fadeIn();
		    	}
		    	else{
		    		$("#popup-caption").show();
		    		$(".popup-alert").fadeIn();
		    	}
		    });
		    $("#control-caption a").click(function(){
		    	box.removeClass("box-caption");
		    	boxTXT.fadeOut();
		    	caption.fadeOut();
		    	$("#control-upload").delay( 600 ).fadeIn();
		    });
		    $("#control-preview .btn-next").click(function(){
		    	$(".popup-alert").addClass("success");
		    	$("#popup-preview").show();
		    	$(".popup-alert").fadeIn();
		    });
		    $("#control-preview .edit").click(function(){
		    	box.removeClass("box-preview");	
			    textarea.fadeIn();	
			    txt.hide();
			    caption.delay( 600 ).fadeIn();
			    preview.fadeOut();
		    });
		    $(".popup-alert .btn-close").click(function(){
		        $(".popup-alert").fadeOut();
		    	$(".popup").delay( 600 ).hide();
		    });
		    $(".popup-alert .mesk").click(function(){
		    	if (!($(".popup-alert").hasClass("success"))) {
			        $(".popup-alert").fadeOut();
			    	$(".popup").delay( 600 ).hide();
		    	}
		    });

		    

        });
    </script>
</head>
<body class="page-landing">
	<?php include 'ssi/header.php'; ?>
	<main>
		<div class="container">
			<h1>ส่งภาพเข้าประกวด</h1>
			<div class="box-photo">
				<form id="formUpload" runat="server">
			        <input type='file' name="upload" id="uploadIMG" />
			        <label for="uploadIMG">
			        	<p>อัพโหลดภาพที่นี่</p>
			        </label>
			        <div class="box-img">
			        	<img id="previewIMG" src="" />
			        </div>
			        <div class="box-text">
			        	<div class="dt-caption">
			        		<textarea placeholder="ตั้งชื่อภาพ"></textarea>
			        		<p>
			        			ใช้มา 10 ปี แล้ว เลือกไม่ผิดจริงๆ<br>
			        			ใช้แล้วคุ้มค่ามาก<br>
			        			อีซูซุ...ใช้แล้วแฮปปี้สุดๆ
			        		</p>
			        	</div>
			        	<p>
			        		จากคุณ
			        		<span>Piyanuch</span>
			        	</p>
			        </div>
			    </form>
			</div>
			<div class="tool-upload">
				<div id="control-upload">
					<a class="reset">ยกเลิก</a>
					<button class="btn-next">ต่อไป</button>
					<ul>
						<li>
							ภาพที่ส่งเข้าประกวดต้องเป็นภาพที่มีรถอีซูซุ รุ่นใดก็ได้ อยู่ภายในภาพ
						</li>
						<li>
							ภาพที่ส่งเข้าประกวดต้องเป็นภาพที่ถ่ายจากกล้องดิจิตอล
						</li>
						<li>
							ความละเอียดไม่ต่ำกว่า 5 ล้านพิกเซล ขนาดภาพไม่ต่ำกว่า 1024 x 768 พิกเซล และต้องเป็นไฟล์ในแบบมาตรฐาน JPEG เท่านั้น
						</li>
					</ul>
				</div>
				<div id="control-caption">
					<p>
						ชื่อภาพสะท้อนความหมายของหัวข้อการประกวด และข้อความไม่เกิน <span>200</span> ตัวอักษร
					</p>
					<a>ย้อนกลับ</a>
					<button class="btn-next">ต่อไป</button>
				</div>
				<div id="control-preview">
					<a class="edit">แก้ไข</a>
					<p>ภาพที่ส่งเข้าประกวดไม่สามารถแก้ไขได้ในภายหลัง<br>กรุณาตรวจสอบความถูกต้องก่อนยืนยันการส่งภาพเข้าประกวด</p>
					<input type="submit" class="btn-next" value="ยืนยัน"></input>
				</div>
			</div>
		</div>
	</main>

	<div class="popup-alert">
	    <div class="mesk"></div>
	    <div class="popup" id="upload-alert">
        	<a class="btn-close"></a>
	    	<p>
	    		คุณยังไม่ได้เลือกภาพ
	    		<b>กรุณาอัพโหลดภาพด้วยค่ะ</b>	    		
	    	</p>
	    </div>
	    <div class="popup" id="popup-caption">
	    	<a class="btn-close"></a>
	    	<p>
	    		คุณยังไม่ได้ตั้งชื่อภาพ
	    		<b>กรุณาตั้งชื่อภาพด้วยค่ะ</b>
	    	</p>
	    </div>
	    <div class="popup" id="popup-preview">
	    	<p>
	    		คุณได้ส่งภาพเข้าร่วมประกวด <span>เรียบร้อยแล้ว</span><br>
	    		ขอบคุณมากค่ะ
	    	</p>
	    	<div class="box-link">
	    		<a class="btn-upload-more" href="upload.php">ส่งภาพประกวดเพิ่ม</a>
	    		<a href="gallery.php">ไปหน้าอัลบั้มภาพทั้งหมด</a>
	    	</div>
	    </div>
    </div>
</body>

</html>